;; Disable stupid welcome screen
(setq inhibit-startup-screen t)

;; Added by Package.el.  This must come before configurations of
;; installed packages.  Don't delete this line.  If you don't want it,
;; just comment it out by adding a semicolon to the start of the line.
;; You may delete these explanatory comments.
(package-initialize)

(require 'package)
(add-to-list 'package-archives '("melpa" . "https://melpa.org/packages/"))
(require 'package)
(add-to-list 'package-archives '("org" . "https://orgmode.org/elpa/") t)
(org-babel-do-load-languages
 'org-babel-load-languages
 '((python . t)))

(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(custom-safe-themes
   (quote
    ("1436d643b98844555d56c59c74004eb158dc85fc55d2e7205f8d9b8c860e177f" default)))
 '(package-selected-packages
   (quote
    (which-key org-bullets org-plus-contrib org evil gruvbox-theme))))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )
(load-theme 'gruvbox t)

;;;;;;;;;;;;;;;;;;;;;;;;
;; General
;;;;;;;;;;;;;;;;;;;;;;;;
(setq make-backup-files nil) ;; disable backup files

(menu-bar-mode -1) ;; disable menu bar
(toggle-scroll-bar -1) ;; disable scroll bar
(tool-bar-mode -1) ;; disable toolbar

(setq-default truncate-lines nil) ;; truncate long lines
(setq-default word-wrap t) ;; wrap words

(set-background-color "#000000") ;; set background color

(setq org-directory "~/megasync")
(setq org-agenda-files '("~/megasync"))

(defun org-file-path(filename)
  "Return the absolute address of an org file, given its relative name."
  (concat (file-name-as-directory org-directory) filename))

(setq org-todo-file "~/megasync/todo.org")
(setq org-school-file "~/megasync/school.org")
(setq org-schedule-file "~/megasync/schedule.org")
(setq org-archive-location
      (concat (org-file-path "archive.org") "::* From %s"))

(defun mark-done-and-archive ()
  "Mark the state of an org-mode item as DONE and archive it."
  (interactive)
  (org-todo 'done)
  (org-archive-subtree))

(define-key org-mode-map (kbd "C-c C-x C-s") 'mark-done-and-archive)
(setq org-log-done 'time)
	


;;;;;;;;;;;;;;;;;;;;
;; Packages
;;;;;;;;;;;;;;;;;;;;
;; the dark side
(require 'evil)
(evil-mode 1)

;; org bullets
(require 'org-bullets)
(add-hook 'org-mode-hook 'org-bullets-mode)

;; which key
(require 'which-key)
(which-key-mode)
(which-key-setup-side-window-bottom)
(setq which-key-idle-delay 1)
(setq which-key-max-description-length 27)
(setq which-key-max-display-columns nil)
(setq which-key-special-keys nil)
